<%@ page import="net.thapelo.chatsa.web.connection.Database" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="javax.management.Query" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Link 808</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script>
        function update(){

            var url="home";


            <%if(request.getParameter("city")!=null  && !request.getParameter("city").isEmpty()){%>
            url+="?city=<%=request.getParameter("city")%>";
            <%if(request.getParameter("area")!=null  && !request.getParameter("area").isEmpty()){%>
            url+="&area=<%=request.getParameter("area")%>";
            <%}}%>
            /*if(city!="null" && city!="" && city!=null) {
                url+="?city="+city;
                if(area!="null") {
                    url+="&area="+area;

                }
            }*/



            $('#chatwindow').load(url+" #chatwindow");
            $('#chatwindow2').load("home #chatwindow2");

            /*var height = 0;
            $('div p').each(function(i, value){
                height += parseInt($(this).height());
            });

            height += '';

            $('chatwindow').animate({scrollTop: height});*/
        }
        setInterval( function(){
            update();
        }, 2000 );
/*

        $('#panel-body2').scrollTop($('#panel-body2')[0].scrollHeight);
*/

    </script>

    <script>

        function myFunction() {
            var input, filter, table, tr, td, i;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("myTable");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    </script>


  <style>

    .list-group.panel > .list-group-item {
      border-bottom-right-radius: 4px;
      border-bottom-left-radius: 4px
    }

    /* Remove the navbar's default margin-bottom and rounded borders */
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }

    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 600px}

    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }

    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }

    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 667px) {
      .sidenav{
        height: auto;
        padding: 15px;
      }

        /*.panel-body2 {
            height: auto;
            padding: 15px;
        }*/
      .row.content {height: auto;}
    }

    /*chat box*/

    /*@media screen and (max-width: 667px) {
        .panel {
            height: auto;
            padding: 15px;
        }
        .row.content {height: auto;}
    }*/

    .chat
    {
      list-style: none;
      margin: 0;
      padding: 0;
    }

    .chat li
    {
      margin-bottom: 10px;
      padding-bottom: 5px;
      border-bottom: 1px dotted #B3A9A9;
    }

    .chat li.left .chat-body
    {
      margin-left: 60px;
    }

    .chat li.right .chat-body
    {
      margin-right: 60px;
    }


    .chat li .chat-body p
    {
      margin: 0;
    }

    .panel .slidedown .glyphicon, .chat .glyphicon
    {

      margin-right: 5px;
    }

    .sidenav
    {
        overflow-y: auto;
    }

    .myCarousel
    {
        overflow-y: auto;
    }

    .panel2
    {
      overflow-y: auto;
      margin-top: 10px;
      height: 590px;
    }




    ::-webkit-scrollbar-track
    {
      -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
      background-color: #F5F5F5;
    }

    ::-webkit-scrollbar
    {
      width: 12px;
      background-color: #F5F5F5;
    }

    ::-webkit-scrollbar-thumb
    {
      -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
      background-color: #555;
    }



    * {
        box-sizing: border-box;
    }

    #myInput {
        background-image: url('/css/searchicon.png');
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 100%;
        font-size: 16px;
        padding: 12px 20px 12px 40px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
    }

    #myTable {
        border-collapse: collapse;
        width: 100%;
        border: 1px solid #ddd;
        font-size: 18px;
    }

    #myTable th, #myTable td {
        text-align: left;
        padding: 12px;
    }

    #myTable tr {
        border-bottom: 1px solid #ddd;
    }

    #myTable tr.header, #myTable tr:hover {
        background-color: #f1f1f1;
    }


  </style>
</head>
<body>

<%

  Database db = new Database();
  db.connect();
  try{

      session.setAttribute("insertDeleteSuccessMessage",null);

      if(session.getAttribute("username")!=null && session.getAttribute("username").equals("admin")){

          Statement stAdd = db.connection.createStatement();
          String qryAdd = null;
          if(request.getParameter("addCity")!=null && !request.getParameter("addCity").isEmpty()) {
              Statement stMx = db.connection.createStatement();
              String qryMx  = "select max(id)+1 as mx from city";
              ResultSet rsMx = stMx.executeQuery(qryMx);
              rsMx.next();

              qryAdd = "insert into city (id,name,type) values ("+rsMx.getString("mx")+",'"+request.getParameter("addCity")+"','"+request.getParameter("roomType")+"')";
          }
          else if(request.getParameter("addArea")!=null && !request.getParameter("addArea").isEmpty() && request.getParameter("addAreaCity")!=null && !request.getParameter("addAreaCity").isEmpty()) {
              Statement stMx = db.connection.createStatement();
              String qryMx  = "select max(id)+1 as mx from area";
              ResultSet rsMx = stMx.executeQuery(qryMx);
              rsMx.next();

              qryAdd = "insert into area (id,name,city_id) values ("+rsMx.getString("mx")+",'"+request.getParameter("addArea")+"',"+request.getParameter("addAreaCity")+")";
          }
          if(qryAdd!=null) {

              stAdd.executeUpdate(qryAdd);
              session.setAttribute("insertDeleteSuccessMessage","Successfully Inserted into Database!");
          }


          Statement stDelete = db.connection.createStatement();
          String qryDelete = null;
          if(request.getParameter("deleteCity")!=null && !request.getParameter("deleteCity").isEmpty()) {
              qryDelete = "delete from city where id="+request.getParameter("deleteCity");
          }
          else if(request.getParameter("deleteArea")!=null && !request.getParameter("deleteArea").isEmpty()) {
              qryDelete = "delete from area where id="+request.getParameter("deleteArea");
          }
          if(qryDelete!=null) {
              stDelete.executeUpdate(qryDelete);
              session.setAttribute("insertDeleteSuccessMessage","Successfully Deleted from Database!");
          }

      }


      if(session.getAttribute("chatCity")!=null) {
          session.setAttribute("chatCity",session.getAttribute("chatCity"));
      }

      if(session.getAttribute("chatArea")!=null) {
          session.setAttribute("chatArea",session.getAttribute("chatArea"));
      }

      if(request.getParameter("city")!=null && !request.getParameter("city").isEmpty()) {

          if(request.getParameter("privateChat")!=null && request.getParameter("privateChat").equals("1")) {

              String firstUser;
              if(request.getParameter("prevCity")!=null && !request.getParameter("prevCity").equals("")) {
                  firstUser = request.getParameter("prevCity").split("-")[0];
              } else {
                  firstUser = request.getParameter("city").split("-")[0];
              }
              Statement stFirstUser = db.connection.createStatement();
              String qryFirstUser = "select * from admin where id=" + firstUser;
              ResultSet rsFirstUser = stFirstUser.executeQuery(qryFirstUser);
              rsFirstUser.next();
              String firstUsername = rsFirstUser.getString("username");

              String secondUser;
              if(request.getParameter("prevCity")!=null && !request.getParameter("prevCity").equals("")) {
                  secondUser = request.getParameter("prevCity").split("-")[1];
              } else {
                  secondUser = request.getParameter("city").split("-")[1];
              }

              Statement stSecondUser = db.connection.createStatement();
              String qrySecondUser = "select * from admin where id=" + secondUser;
              ResultSet rsSecondUser = stSecondUser.executeQuery(qrySecondUser);
              rsSecondUser.next();
              String secondUsername = rsSecondUser.getString("username");

              if(session.getAttribute("userId").equals(firstUser)) {
                  session.setAttribute("chatCity", secondUsername+" (private chat room)");
              } else {
                  session.setAttribute("chatCity", firstUsername+" (private chat room)");
              }




              if(request.getParameter("prevCity")==null || request.getParameter("prevCity").equals("")) {

                  Statement stCheck = db.connection.createStatement();
                  String qryCheck = "select id from city where type='private' and name='" + request.getParameter("city")+"'";
                  ResultSet rsCheck = stCheck.executeQuery(qryCheck);

                  if (!rsCheck.next()) {

                      Statement stMx = db.connection.createStatement();
                      String qryMx = "select max(id)+1 as mx from city";
                      ResultSet rsMx = stMx.executeQuery(qryMx);
                      rsMx.next();

                      Statement stInsert = db.connection.createStatement();
                      String qryInsert = "insert into city (id,name,type) values(" + rsMx.getString("mx") + ",'" + request.getParameter("city") + "','private')";
                      stInsert.executeUpdate(qryInsert);

                      response.sendRedirect("home?privateChat=1&city=" + rsMx.getString("mx") + "&prevCity=" + request.getParameter("city"));
                  } else {
                      response.sendRedirect("home?privateChat=1&city=" + rsCheck.getString("id") + "&prevCity=" + request.getParameter("city"));
                  }
              }
          }

          else {
              Statement st = db.connection.createStatement();
              String qry = "select * from city where id=" + request.getParameter("city");
              ResultSet rs = st.executeQuery(qry);
              rs.next();
              if(!(request.getParameter("message")!=null && !request.getParameter("message").isEmpty() && request.getParameter("sessionId")!=null && !request.getParameter("sessionId").isEmpty())) {
                  session.setAttribute("chatCity", rs.getString("name"));
              }
          }
      }
      if(request.getParameter("area")!=null && !request.getParameter("area").isEmpty()) {
          Statement st = db.connection.createStatement();
          String qry= "select * from area where id="+request.getParameter("area");
          ResultSet rs = st.executeQuery(qry);
          rs.next();
          session.setAttribute("chatArea",rs.getString("name"));
      }

      if(session.getAttribute("sessionId")==null){
          Statement st = db.connection.createStatement();
          String qry = "select * from session where status=0";
          ResultSet rs = st.executeQuery(qry);
          if(rs.next()) {
              Statement st2 = db.connection.createStatement();
              String qry2 = "update session set status=1 where id="+rs.getString("id");
              session.setAttribute("sessionId",rs.getString("id"));
              st2.executeUpdate(qry2);
          } else {
              Statement st0 = db.connection.createStatement();
              String qry0 = "select max(id)+1 as mx from session";
              ResultSet rs0 = st0.executeQuery(qry0);
              rs0.next();
              Statement st2 = db.connection.createStatement();
              String qry2 = "insert into session (id,status) VALUES ("+rs0.getString("mx")+",1)";
              session.setAttribute("sessionId",rs0.getString("mx"));
              st2.executeUpdate(qry2);
          }

      } else {
          session.setAttribute("sessionId",session.getAttribute("sessionId").toString());
      }



      if(request.getParameter("screenName")!=null && !request.getParameter("screenName").isEmpty())
      {
          session.setAttribute("screenName",request.getParameter("screenName"));
      }
      if(request.getParameter("goAnonymous")!=null && !request.getParameter("goAnonymous").isEmpty())
      {
          session.setAttribute("screenName",null);
      }

      if(session.getAttribute("screenName")==null && session.getAttribute("anonymousId")==null) {
          Statement st = db.connection.createStatement();
          String qry = "select * from anonymous where status=0";
          ResultSet rs = st.executeQuery(qry);
          if(rs.next()) {
              Statement st2 = db.connection.createStatement();
              String qry2 = "update anonymous set status=1 where id="+rs.getString("id");
              session.setAttribute("anonymousId",rs.getString("id"));
              st2.executeUpdate(qry2);
          }
          else{
              Statement st0 = db.connection.createStatement();
              String qry0 = "select max(id)+1 as mx from anonymous";
              ResultSet rs0 = st0.executeQuery(qry0);
              rs0.next();
              Statement st2 = db.connection.createStatement();
              String qry2 = "insert into anonymous (id,status) VALUES ("+rs0.getString("mx")+",1)";
              session.setAttribute("anonymousId",rs0.getString("mx"));
              st2.executeUpdate(qry2);
          }
      }





      if(request.getParameter("message")!=null && !request.getParameter("message").isEmpty() && request.getParameter("sessionId")!=null && !request.getParameter("sessionId").isEmpty()) {

          Statement st0 = db.connection.createStatement();
          String qry0= "select max(id)+1 as mx from chat";
          ResultSet rs0 = st0.executeQuery(qry0);
          rs0.next();

          String id = rs0.getString("mx");
          String city = "0";
          if(request.getParameter("city")!=null && !request.getParameter("city").isEmpty()) {
              city = request.getParameter("city");
          }
          String area = "0";
          if(request.getParameter("area")!=null && !request.getParameter("area").isEmpty()) {
              area = request.getParameter("area");
          }
          String message = request.getParameter("message");
          String sessionId = session.getAttribute("sessionId").toString();
          String name;
          if(session.getAttribute("screenName")!=null){
              name=session.getAttribute("screenName").toString();
          } else {
              name="ano"+session.getAttribute("anonymousId").toString();
          }

          Statement st = db.connection.createStatement();
          String qry= "insert into chat (id,city,area,message,session_id,name) values("+id+","+city+","+area+",'"+message+"',"+sessionId+",'"+name+"')";
          st.executeUpdate(qry);

      }













%>




<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">

    <div class="item active">
      <img src="resources/images/cor111.jpg" alt="Los Angeles" style="width:100%;">
      <div class="carousel-caption"  style="color: #1a1a1a">
        <%--<h3>LIN<span style="color: yellowgreen">K</span> 8<span style="color: #8DC26F;">0</span>8</h3>--%>
        <h4 style="margin-top: 0px;margin-bottom: 0px;">CHAT PEOPLE WORLDWIDE</h4>
        <p style="margin-top: 0px;margin-bottom: 0px;">chatting with others is always so much fun!</p>
      </div>
    </div>

    <div class="item">
      <img src="resources/images/cor222.jpg" alt="Chicago" style="width:100%;">
      <div class="carousel-caption">
        <%--<h3>LIN<span style="color: yellowgreen">K</span> 8<span style="color: #8DC26F;">0</span>8</h3>--%>
        <h4 style="margin-top: 0px;margin-bottom: 0px;">CHAT ANONYMOUSLY</h4>
        <p style="margin-top: 0px;margin-bottom: 0px;">chat with others without letting them know about your identity!</p>
      </div>
    </div>

    <div class="item">
      <img src="resources/images/cor333.jpg" alt="New York" style="width:100%;">
      <div class="carousel-caption">
        <%--<h3>LIN<span style="color: yellowgreen">K</span> 8<span style="color: #8DC26F;">0</span>8</h3>--%>
        <h4 style="margin-top: 0px;margin-bottom: 0px;">HELLO SOUTH AFRICA</h4>
        <p style="margin-top: 0px;margin-bottom: 0px;">chat in different cities and areas as if you are there!</p>
      </div>
    </div>

  </div>
  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<%--<img class="img-responsive" src="resources/images/head2.jpg" alt="image" width="100%">--%>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="home">LIN<span style="color: yellowgreen">K</span> 8<span style="color: #8DC26F;">0</span>8</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="home">Home</a></li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <%
          if (session.getAttribute("userId") != null) {
        %>
        <li>
          <form action="login" method="post">
            <span style="color: #8DC26F;" class="glyphicon glyphicon-user"></span>&nbsp;<span style="color: white"><b style="color: white;">logged in as <%=session.getAttribute("username")%></b></span>
            <input name="logout" type="hidden" value="logout">
            <button type="submit" class="btn btn-danger navbar-btn"><span class="glyphicon glyphicon-log-out"></span> Log out</button>
          </form>
        </li>
        <%
        } else {
        %>
          <ul class="nav navbar-nav navbar-right">
        <li><a href="login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>
        <%}%>
      </ul>
    </div>
  </div>
</nav>


<div class="container-fluid">
  <div class="row content">


      <%if(session.getAttribute("insertDeleteSuccessMessage")!=null) {%>
      <div class="alert alert-success alert-dismissable fade in" style="margin-left: 20px; margin-right: 20px; text-align: center">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success!</strong> <%=session.getAttribute("insertDeleteSuccessMessage").toString()%>
      </div>
      <%}%>

    <div class="col-sm-3 sidenav">


        <%
        if(session.getAttribute("username")!=null && !session.getAttribute("username").toString().equals("admin")) {
        %>


        <br>
        <legend>MY CHAT HISTORY</legend>
        <div class="panel-group">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#collapseChatHistory">chat with friends</a>
                    </h4>
                </div>
                <div id="collapseChatHistory" class="panel-collapse collapse">
                    <div class="panel-body">
                        <%
                            Statement st = db.connection.createStatement();
                            String qry = "select * from city where type='private' and (name like '%-"+session.getAttribute("userId").toString()+"' or name like '"+session.getAttribute("userId").toString()+"-%')";
                            ResultSet rsCities = st.executeQuery(qry);
                            while (rsCities.next()) {
                                String cityName = rsCities.getString("name");
                                String cityLink = "home?privateChat=1&city="+cityName;

                                String userToChat;

                                if(cityName.split("-")[0].equals(session.getAttribute("userId").toString())) {
                                    userToChat = cityName.split("-")[1];
                                } else {
                                    userToChat = cityName.split("-")[0];
                                }

                                Statement stU = db.connection.createStatement();
                                String qryU = "select * from admin where id="+userToChat;
                                ResultSet rsU = stU.executeQuery(qryU);
                                rsU.next();


                        %>
                        <h4 class="panel-title"><%=rsU.getString("username")%><a href="<%=cityLink%>" style="color: darkblue; float: right;">private chat</a></h4>
                        <%
                            }
                        %>
                    </div>
                    <%--<div class="panel-footer"><button type="button" class="btn btn-success center-block">Success</button></div>--%>
                </div>
            </div>
        </div>



        <br>
        <legend>USERS</legend>
        <div class="panel-group">
            <%
                Statement stUser = db.connection.createStatement();
                String qryUser = "select * from admin where username!='admin' and id>0";
                ResultSet rsUser = stUser.executeQuery(qryUser);

            %>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#collapseUsers">show all users</a>
                </h4>
            </div>
            <div id="collapseUsers" class="panel-collapse collapse" size="4">
                <div class="panel-body">
                    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name">

                    <table id="myTable">
                        <tr class="header">
                            <th style="width:60%;">Name</th>
                            <th style="width:40%;"></th>
                        </tr>
                        <%
                            while (rsUser.next()) {
                                String username = rsUser.getString("username");
                                String userId = rsUser.getString("id");
                                String userLink;
                                if(userId.compareTo(session.getAttribute("userId").toString())<0) {
                                    userLink = "home?privateChat=1&city=" + userId + "-" + session.getAttribute("userId").toString();
                                } else {
                                    userLink = "home?privateChat=1&city=" + session.getAttribute("userId").toString() + "-" + userId;
                                }
                        %>
                        <tr>

                            <td><%=username%></td>
                            <td><a href="<%=userLink%>" style="color: darkblue; float: right;">private chat</a></td>

                        </tr>
                        <%}%>
                    </table>
                </div>
                <%--<div class="panel-footer"><button type="button" class="btn btn-success center-block">Success</button></div>--%>
            </div>
        </div>
        </div>
        <%
                }
        %>


<br>
<legend>CITIES</legend>
      <div class="panel-group">
      <%
      Statement st = db.connection.createStatement();
      String qry = "select * from city where type='city'";
      ResultSet rsCities = st.executeQuery(qry);
      while (rsCities.next()) {
          String cityName = rsCities.getString("name");
          String cityId = rsCities.getString("id");
          String cityLink = "home?city="+cityId;
      %>

        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" href="#collapse<%=cityId%>"><%=cityName%></a><a href="<%=cityLink%>" style="color: darkblue; float: right;">enter room</a>
            </h4>
          </div>
          <div id="collapse<%=cityId%>" class="panel-collapse collapse">
            <div class="panel-body">
              <%
                Statement st2 = db.connection.createStatement();
                String qry2 = "select * from area where city_id="+cityId;
                ResultSet rsAreas = st2.executeQuery(qry2);
                while (rsAreas.next()) {
                  String areaName = rsAreas.getString("name");
                  String areaId = rsAreas.getString("id");
                  String areaLink = cityLink +"&area="+areaId;
              %>
              <h4 class="panel-title"><%=areaName%><a href="<%=areaLink%>" style="color: darkblue; float: right;">enter room</a></h4>
              <%
                }
              %>
            </div>
            <%--<div class="panel-footer"><button type="button" class="btn btn-success center-block">Success</button></div>--%>
          </div>
        </div>

        <%
          }
        %>
      </div>




        <br>
        <legend>UNIVERSITIES</legend>
        <div class="panel-group">
            <%
                Statement stUniv = db.connection.createStatement();
                String qryUniv = "select * from city where type='university'";
                ResultSet rsUniv = stUniv.executeQuery(qryUniv);
                while (rsUniv.next()) {
                    String univName = rsUniv.getString("name");
                    String univId = rsUniv.getString("id");
                    String univLink = "home?city="+univId;
            %>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#collapse<%=univId%>"><%=univName%></a><a href="<%=univLink%>" style="color: darkblue; float: right;">enter room</a>
                    </h4>
                </div>
            </div>

            <%
                }
            %>
        </div>

        <br>
        <legend>GENERAL ROOMS</legend>
        <div class="panel-group">
            <%
                Statement stGeneral = db.connection.createStatement();
                String qryGeneral = "select * from city where type='general'";
                ResultSet rsGeneral = stGeneral.executeQuery(qryGeneral);
                while (rsGeneral.next()) {
                    String generalName = rsGeneral.getString("name");
                    String generalId = rsGeneral.getString("id");
                    String generalLink = "home?city="+generalId;
            %>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" href="#collapse<%=generalId%>"><%=generalName%></a><a href="<%=generalLink%>" style="color: darkblue; float: right;">enter room</a>
                    </h4>
                </div>
            </div>

            <%
                }
            %>
        </div>


<br>
      <%if(session.getAttribute("screenName")==null){%>
      <p>Set Up Your Screen Name</p>
      <%}else{%>
      <form method="get" action="home">
        <div class="input-group">
          <input type="hidden" name="goAnonymous" value="1"/>

          <button class="btn btn-link" type="submit">
            Go Anonymous
          </button>
        </div>
      </form>

      <p>Screen Name: <%=session.getAttribute("screenName").toString()%></p>
      <p>Change Your Screen Name</p>
      <%}%>
      <form method="get" action="home">
      <div class="input-group">
        <input type="text" name="screenName" class="form-control" placeholder="Your Name">
        <span class="input-group-btn">
          <button class="btn btn-default" type="submit">
            <span class="glyphicon glyphicon-user"></span>&nbsp;Submit
          </button>
        </span>
      </div>
      </form>

        <%if(session.getAttribute("username")!=null && session.getAttribute("username").equals("admin")) {%>

        <br>
        <p>Insert A New City</p>
        <form method="post" action="home">
            <div class="input-group">
                <input type="hidden" name="roomType" value="city"/>
                <input type="text" name="addCity" class="form-control" placeholder="City Name" required>
                <span class="input-group-btn">
          <button class="btn btn-default" type="submit">
            <span class="glyphicon glyphicon-plus"></span>&nbsp;Submit
          </button>
        </span>
            </div>
        </form>
        <br>
        <p>Insert A New Area</p>
        <form method="get" action="home">
            <div class="input-group">
                <%
                Statement stAdd = db.connection.createStatement();
                String qryAdd = "select * from city where type='city'";
                ResultSet rsAdd = stAdd.executeQuery(qryAdd);
                %>
                <select size="3" name="addAreaCity" class="form-control" required>
                    <option value="null" disabled>---Select City---</option>
                    <%while (rsAdd.next()){%>
                    <option value="<%=rsAdd.getString("id")%>">
                        <%=rsAdd.getString("name")%>
                    </option>
                    <%}%>
                </select>
                <input type="text" name="addArea" class="form-control" placeholder="Area Name" required>
                <span class="input-group-btn">
          <button class="btn btn-default" type="submit">
            <span class="glyphicon glyphicon-plus"></span>&nbsp;Submit
          </button>
        </span>
            </div>
        </form>



        <br>
        <p>Delete A City</p>
        <form method="get" action="home">
            <div class="input-group">
                <%
                    Statement stDelete = db.connection.createStatement();
                    String qryDelete = "select * from city where type='city'";
                    ResultSet rsDelete = stDelete.executeQuery(qryDelete);
                %>
                <input type="hidden" name="roomType" value="city"/>
                <select size="3" name="deleteCity" class="form-control" required>
                    <option value="null" disabled>---Select City---</option>
                    <%while (rsDelete.next()){%>
                    <option value="<%=rsDelete.getString("id")%>">
                        <%=rsDelete.getString("name")%>
                    </option>
                    <%}%>
                </select>
                <span class="input-group-btn">
          <button class="btn btn-default" type="submit">
            <span class="glyphicon glyphicon-minus"></span>&nbsp;Submit
          </button>
        </span>
            </div>
        </form>


        <br>
        <p>Delete An Area</p>
        <form method="get" action="home">
            <div class="input-group">
                <%
                    Statement stDelete2 = db.connection.createStatement();
                    String qryDelete2 = "select * from area";
                    ResultSet rsDelete2 = stDelete2.executeQuery(qryDelete2);
                %>
                <select size="3" name="deleteArea" class="form-control" required>
                    <option value="null" disabled>---Select Area---</option>
                    <%while (rsDelete2.next()){

                        Statement stDelete3 = db.connection.createStatement();
                        String qryDelete3 = "select * from city where id="+rsDelete2.getString("city_id");
                        ResultSet rsDelete3 = stDelete3.executeQuery(qryDelete3);
                        rsDelete3.next();

                    %>
                    <option value="<%=rsDelete2.getString("id")%>">
                        <%=rsDelete2.getString("name")%>,&nbsp;<%=rsDelete3.getString("name")%>
                    </option>
                    <%}%>
                </select>
                <span class="input-group-btn">
          <button class="btn btn-default" type="submit">
            <span class="glyphicon glyphicon-minus"></span>&nbsp;Submit
          </button>
        </span>
            </div>
        </form>

        <br>
        <p>Insert A New University</p>
        <form method="post" action="home">
            <div class="input-group">
                <input type="hidden" name="roomType" value="university"/>
                <input type="text" name="addCity" class="form-control" placeholder="University Name" required>
                <span class="input-group-btn">
          <button class="btn btn-default" type="submit">
            <span class="glyphicon glyphicon-plus"></span>&nbsp;Submit
          </button>
        </span>
            </div>
        </form>

        <br>
        <p>Delete An University</p>
        <form method="get" action="home">
            <div class="input-group">
                <%
                    Statement stDeleteUniv = db.connection.createStatement();
                    String qryDeleteUniv = "select * from city where type='university'";
                    ResultSet rsDeleteUniv = stDeleteUniv.executeQuery(qryDeleteUniv);
                %>
                <input type="hidden" name="roomType" value="city"/>
                <select size="3" name="deleteCity" class="form-control" required>
                    <option value="null" disabled>---Select University---</option>
                    <%while (rsDeleteUniv.next()){%>
                    <option value="<%=rsDeleteUniv.getString("id")%>">
                        <%=rsDeleteUniv.getString("name")%>
                    </option>
                    <%}%>
                </select>
                <span class="input-group-btn">
          <button class="btn btn-default" type="submit">
            <span class="glyphicon glyphicon-minus"></span>&nbsp;Submit
          </button>
        </span>
            </div>
        </form>





        <br>
        <p>Insert A New General Room</p>
        <form method="post" action="home">
            <div class="input-group">
                <input type="hidden" name="roomType" value="general"/>
                <input type="text" name="addCity" class="form-control" placeholder="General Room Name" required>
                <span class="input-group-btn">
          <button class="btn btn-default" type="submit">
            <span class="glyphicon glyphicon-plus"></span>&nbsp;Submit
          </button>
        </span>
            </div>
        </form>


        <br>
        <p>Delete A General Room</p>
        <form method="get" action="home">
            <div class="input-group">
                <%
                    Statement stDeleteGeneral = db.connection.createStatement();
                    String qryDeleteGeneral = "select * from city where type='general'";
                    ResultSet rsDeleteGeneral = stDeleteGeneral.executeQuery(qryDeleteGeneral);
                %>
                <input type="hidden" name="roomType" value="city"/>
                <select size="3" name="deleteCity" class="form-control" required>
                    <option value="null" disabled>---Select General Room---</option>
                    <%while (rsDeleteGeneral.next()){%>
                    <option value="<%=rsDeleteGeneral.getString("id")%>">
                        <%=rsDeleteGeneral.getString("name")%>
                    </option>
                    <%}%>
                </select>
                <span class="input-group-btn">
          <button class="btn btn-default" type="submit">
            <span class="glyphicon glyphicon-minus"></span>&nbsp;Submit
          </button>
        </span>
            </div>
        </form>

        <%}%>
        <br>
    </div>



        <div class="col-md-9"  >
          <div class="panel2 panel-primary">

            <div class="panel-heading">

              <span class="glyphicon glyphicon-comment"></span> Chatroom: <%if(session.getAttribute("chatCity")==null && session.getAttribute("chatArea")==null){%>Global<%}%><%if(session.getAttribute("chatArea")!=null){%><%=session.getAttribute("chatArea")%>, <%}%><%if(session.getAttribute("chatCity")!=null){%><%=session.getAttribute("chatCity")%><%}%>
            </div>

              <div class="panel-body2" >
              <div id="chatwindow">
                <ul class="chat">
                    <%
                        Statement stChat = db.connection.createStatement();
                        String qryChat = "select * from chat WHERE id>0";
                        if(request.getParameter("city")!=null && !request.getParameter("city").isEmpty()) {
                            qryChat+=" and city="+request.getParameter("city");
                        } else {
                            qryChat+=" and city=0";
                        }
                        if(request.getParameter("area")!=null && !request.getParameter("area").isEmpty()) {
                            qryChat+=" and area="+request.getParameter("area");
                        } else {
                            qryChat+=" and area=0";
                        }

                        qryChat+=" order by(id) desc";

                        ResultSet rsChat = stChat.executeQuery(qryChat);


                        while (rsChat.next()) {


                            if(rsChat.getString("session_id")!=null && rsChat.getString("session_id").equals(session.getAttribute("sessionId").toString())) {
                    %>
                    <li class="right clearfix"><span class="chat-img pull-right">
                            <img src="http://placehold.it/50/FA6F57/fff&text=ME" alt="User Avatar" class="img-circle" />
                        </span>
                        <div class="chat-body clearfix">
                            <div class="header">
                                <small class=" text-muted"><span class="glyphicon glyphicon-time"></span><%=rsChat.getString("datetime").substring(0,19)%></small>
                                <strong class="pull-right primary-font"><%=rsChat.getString("name")/*.substring(0,Math.min(18,rsChat.getString("name").length()))*/%></strong>
                            </div>
                            <br>
                            <p style="float: right;">
                                <%=rsChat.getString("message")%>
                            </p>
                        </div>
                    </li>
                    <%}else{%>

                    <li class="left clearfix"><span class="chat-img pull-left">
                            <img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" />
                        </span>
                        <div class="chat-body clearfix">
                            <div class="header">
                                <strong class="primary-font"><%=rsChat.getString("name")/*.substring(0,Math.min(18,rsChat.getString("name").length()))*/%></strong> <small class="pull-right text-muted">
                                <span class="glyphicon glyphicon-time"></span><%=rsChat.getString("datetime").substring(0,19)%></small>
                            </div>
                            <br>
                            <p>
                                <%=rsChat.getString("message")%>
                            </p>
                        </div>
                    </li>
                     <%
                             } }
                     %>

                </ul>
              </div>
              </div>


          </div>

            <form method="post" action="home">
                <div class="panel-footer">
                    <div class="input-group">
                        <%if(request.getParameter("city")!=null && !request.getParameter("city").isEmpty()){%>
                        <input type="hidden" name="city" value="<%=request.getParameter("city")%>"/>
                        <%}
                            if(request.getParameter("area")!=null && !request.getParameter("area").isEmpty()){%>
                        <input type="hidden" name="area" value="<%=request.getParameter("area")%>"/>
                        <%}%>

                        <input type="hidden" name="sessionId" value="<%=session.getAttribute("sessionId")%>"/>
                        <input id="btn-input" type="text" class="form-control input-sm" placeholder="Type your message here..." name="message"/>
                        <span class="input-group-btn">
                            <button class="btn btn-warning btn-sm" id="btn-chat" type="submit">
                                Send</button>
                        </span>
                    </div>
                </div>
            </form>
    <%--<div class="col-sm-9">
      <h4><small>RECENT POSTS</small></h4>
      <hr>
      <h2>I Love Food</h2>
      <h5><span class="glyphicon glyphicon-time"></span> Post by Jane Dane, Sep 27, 2015.</h5>
      <h5><span class="label label-danger">Food</span> <span class="label label-primary">Ipsum</span></h5><br>
      <p>Food is my passion. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
      <br><br>

      <h4><small>RECENT POSTS</small></h4>
      <hr>
      <h2>Officially Blogging</h2>
      <h5><span class="glyphicon glyphicon-time"></span> Post by John Doe, Sep 24, 2015.</h5>
      <h5><span class="label label-success">Lorem</span></h5><br>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
      <hr>

      <h4>Leave a Comment:</h4>
      <form role="form">
        <div class="form-group">
          <textarea class="form-control" rows="3" required></textarea>
        </div>
        <button type="submit" class="btn btn-success">Submit</button>
      </form>
      <br><br>

      <p><span class="badge">2</span> Comments:</p><br>

      <div class="row">
        <div class="col-sm-2 text-center">
          <img src="bandmember.jpg" class="img-circle" height="65" width="65" alt="Avatar">
        </div>
        <div class="col-sm-10">
          <h4>Anja <small>Sep 29, 2015, 9:12 PM</small></h4>
          <p>Keep up the GREAT work! I am cheering for you!! Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
          <br>
        </div>
        <div class="col-sm-2 text-center">
          <img src="bird.jpg" class="img-circle" height="65" width="65" alt="Avatar">
        </div>
        <div class="col-sm-10">
          <h4>John Row <small>Sep 25, 2015, 8:25 PM</small></h4>
          <p>I am so happy for you man! Finally. I am looking forward to read about your trendy life. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
          <br>
          <p><span class="badge">1</span> Comment:</p><br>
          <div class="row">
            <div class="col-sm-2 text-center">
              <img src="bird.jpg" class="img-circle" height="65" width="65" alt="Avatar">
            </div>
            <div class="col-xs-10">
              <h4>Nested Bro <small>Sep 25, 2015, 8:28 PM</small></h4>
              <p>Me too! WOW!</p>
              <br>
            </div>
          </div>
        </div>
      </div>
    </div>--%>
  </div>

      <footer class="footer2 col col-sm-12" style="text-align: center;">
          <p>LIN<span style="color: yellowgreen">K</span> 8<span style="color: #8DC26F;">0</span>8</p>
          <p>web developed by: Md. Al- Amin</p>
      </footer>


</div>

</div>


<%
    }catch (Exception e) {

    } finally{
      db.close();
    }

    %>
</body>
</html>
