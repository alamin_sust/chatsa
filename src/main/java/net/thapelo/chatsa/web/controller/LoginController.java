package net.thapelo.chatsa.web.controller;


import net.thapelo.chatsa.web.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Al-Amin on 3/9/2017.
 */
@Controller
public class LoginController {

    @Autowired
    LoginService loginService;

    private static final String LOGIN = "login";
    private static final String HOME = "home";
    private static final String REDIRECT_HOME = "redirect:/home";

    @GetMapping("/login")
    public String loadLoginPage(HttpSession session) {
        if(session.getAttribute("userId")!=null)
        {
            return REDIRECT_HOME;
        }
        session.setAttribute("loginMsg", null);
        return LOGIN;
    }

    @PostMapping("/login")
    public String login(@RequestParam(value = "username", required = false) String username,
                        @RequestParam(value = "password", required = false) String password,
                        @RequestParam(value = "logout", required = false) String logout,
                        @RequestParam(value = "register", required = false) String register,
                        HttpSession session) throws SQLException {

        /*pass reset from login page*/
        /*String hashedPass = EncryptionUtil.generateSecuredHash(password);
        if(username!=null && hashedPass !=null) {
            loginService.setPass(username, hashedPass);
        }*/

        session.setAttribute("loginMsg", null);
        session.setAttribute("chatCity",null);
        session.setAttribute("chatArea", null);

        if(logout!=null) {
            session.setAttribute("userId",null);
            session.setAttribute("username",null);
            session.setAttribute("chatCity",null);
            session.setAttribute("chatArea", null);
            return HOME;
        }

        if(register!=null&& !register.isEmpty()) {
            if (loginService.register(username, password, session) /*&& MacBindingUtil.isAuthorizedMac()*/) {
                session.setAttribute("loginMsg", "Successfuly Registerd!");
            } else {
                session.setAttribute("loginMsg", "Username taken. Try with another username.");
            }
        }
        else {
            if (loginService.login(username, password, session) /*&& MacBindingUtil.isAuthorizedMac()*/) {
                return REDIRECT_HOME;
            } else {
                session.setAttribute("loginMsg", "Username and Password doesn't match. Try again.");
            }
        }

        return LOGIN;

    }

    @GetMapping("/leveledSignup")
    public String loadLeveledSignupPage() {
        return "leveledSignup";
    }


}
