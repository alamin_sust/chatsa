package net.thapelo.chatsa.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;

/**
 * Created by Al-Amin on 4/23/2017.
 */
@Controller
public class All {

    private static final String HOME = "home";
    private static final String REDIRECT_HOME = "redirect:/home";

    @GetMapping("/")
    public String loadHomePageRedirect(HttpSession session) {
        return REDIRECT_HOME;
    }

    @GetMapping("/home")
    public String loadHomePage(HttpSession session) {
        return HOME;
    }

    @PostMapping("/home")
    public String loadHomePagePost(HttpSession session) {
        return HOME;
    }

}
