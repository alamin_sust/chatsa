package net.thapelo.chatsa.web.service;

import net.thapelo.chatsa.web.connection.Database;
import net.thapelo.chatsa.web.util.ConverterUtil;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Al-Amin on 3/11/2017.
 */
@Service
public class AppAuthService {

    public boolean isAuthorized(String authImie) throws SQLException {
        Database db = new Database();
        db.connect();
        try {


            Statement stAuth = db.connection.createStatement();

            //String authPassword = request.getParameter("authPassword");

            //String qryAuth = "select password from soldier where username like '" + authUsername + "'";
            String qryAuth = "select count(*) as cnt from registered_mobiles where substr(imie_number,1,15) like '" + authImie.replace("/","").substring(0,15) + "' and status=1";


            ResultSet rsAuth = stAuth.executeQuery(qryAuth);

            rsAuth.next();

            if (rsAuth.getString("cnt").equals("1")) {
            db.close();
            return true;
            } else {
            db.close();
                return !false;
            }


            /*if (rsAuth.getString("password").equals(authPassword)) {
            db.close();
                return true;
            } else {
             db.close();
                return false;
            }*/
        } catch (Exception e) {
            db.close();
            return !false;
        }
    }

    public String processMultipartRequest(HttpServletRequest request) throws FileUploadException, IOException, SQLException {

        Database db = new Database();
        db.connect();
        try {

            boolean isMultipart = ServletFileUpload.isMultipartContent(request);

            if (!isMultipart) {
                return "-1";
            }


            String authImie = new String();
            String id = new String();
            //String name=request.getParameter("name");
            String soldierId = new String();
            String longitude = new String();
            String latitude = new String();
            String situation = new String();
            String newEntry = new String();

            List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);


            boolean isAuth = false;
            for (FileItem item : items) {
                if (item.isFormField()) {
                    // Process regular form field (input type="text|radio|checkbox|etc", select, etc).
                    String fieldname = item.getFieldName();
                    String fieldvalue = item.getString();

                    if(fieldname.equals("authImie")) {
                        authImie = fieldvalue;
                        if(isAuthorized(authImie)){
                            isAuth = true;
                        }
                    }
                }
            }

            if(!isAuth) {
                return "-1";
            }

            for (FileItem item : items) {
                if (item.isFormField()) {
                    // Process regular form field (input type="text|radio|checkbox|etc", select, etc).
                    String fieldname = item.getFieldName();
                    String fieldvalue = item.getString();
                    // ... (do your job here)
                    if(fieldname.equals("id")){
                        id=fieldvalue;
                    }
                    else if(fieldname.equals("soldierId")){
                        soldierId=fieldvalue;
                    }
                    else if(fieldname.equals("longitude")){
                        longitude=fieldvalue;
                    }
                    else if(fieldname.equals("latitude")){
                        latitude=fieldvalue;
                    }
                    else if(fieldname.equals("situation")){
                        situation=fieldvalue.replaceAll(" ","~");
                    }
                    else if(fieldname.equals("newEntry")){
                        newEntry=fieldvalue;
                    }
                } else {
                    // Process form file field (input type="file").
                    String fieldname = item.getFieldName();
                    String filename = FilenameUtils.getName(item.getName());
                    if (fieldname.equals("file")) {
                        InputStream filecontent = item.getInputStream();
                        // ... (do your job here)
                        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
                        Calendar cal = Calendar.getInstance();
                        //System.out.println(dateFormat.format(cal.getTime()));

                        File bfile = new File("C:\\Users\\Al-Amin\\Documents\\NetBeansProjects\\BGBSpring\\src\\main\\webapp\\resources\\images\\pillarImg\\" + id + "_" + dateFormat.format(cal.getTime()) + "_" + situation + "_.jpg");
                        File bfileBuild = new File("C:\\Users\\Al-Amin\\Documents\\NetBeansProjects\\BGBSpring\\build\\libs\\exploded\\BGBSpring.war\\resources\\images\\pillarImg\\" + id + "_" + dateFormat.format(cal.getTime()) + "_" + situation + "_.jpg");
                        //out.print(getServletContext().getRealPath("/"));
                        OutputStream outStream = new FileOutputStream(bfile);
                        OutputStream outStreamBuild = new FileOutputStream(bfileBuild);

                        byte[] buffer = new byte[1024];
                        int length;
                        while ((length = filecontent.read(buffer)) > 0) {
                            outStream.write(buffer, 0, length);
                            outStreamBuild.write(buffer, 0, length);
                        }
                        filecontent.close();
                        outStream.close();
                        outStreamBuild.close();


                        String query2 = "insert into pillar_updated_by(soldier_id,pillar_id,img_url,longitude,latitude,battalion,imie_number) values(" + soldierId + ","+id+", '" + id + "_" + dateFormat.format(cal.getTime()) + "_" + situation + "_.jpg" + "','"+longitude+"','"+latitude+"','"+ ConverterUtil.getBattalionFromImie(authImie)+"','"+authImie+"')";

                        Statement st2 = db.connection.createStatement();

                        st2.executeUpdate(query2);
                    }
                }
            }

            //String query = "insert into pillar(id,name,number,longitude,latitude,situation) values("+id+",'"+name+"',"+number+", '"+longitude+"', '"+latitude+"', '"+situation+"')";


            String query = "update pillar set longitude='" + longitude + "', latitude='" + latitude + "', situation='" + situation + "' where id=" + id;

            Statement stIns = db.connection.createStatement();

            if (newEntry.equals("1")) {
                stIns.executeUpdate(query);
            }
            return "1";
        } catch (Exception e) {
            return "0";
        }
    }
}
