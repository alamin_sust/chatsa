package net.thapelo.chatsa.web.service;

import net.thapelo.chatsa.web.dao.AdminDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.sql.SQLException;

/**
 * Created by Al-Amin on 3/9/2017.
 */
@Service
public class LoginService {

    @Autowired
    AdminDao adminDao;

    public boolean login(String username, String password, HttpSession session) throws SQLException {
        return adminDao.login(username,password,session);
    }

    public void setPass(String username, String hashedPass) throws SQLException {
        adminDao.setPass(username,hashedPass);
        return;
    }

    public boolean register(String username, String password, HttpSession session) throws SQLException {
        return adminDao.register(username,password,session);
    }
}
