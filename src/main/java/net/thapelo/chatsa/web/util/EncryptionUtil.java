package net.thapelo.chatsa.web.util;

import org.mindrot.jbcrypt.BCrypt;

public class EncryptionUtil {

    public static String generateSecuredHash(String originalString) {
        return BCrypt.hashpw(originalString, BCrypt.gensalt(12));
    }

    public static boolean matchWithSecuredHash(String providedString, String existingHash) {
        return BCrypt.checkpw(providedString, existingHash);
    }
}
