package net.thapelo.chatsa.web.dao;

import net.thapelo.chatsa.web.connection.Database;
import net.thapelo.chatsa.web.util.EncryptionUtil;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpSession;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Al-Amin on 3/9/2017.
 */
@Repository
public class AdminDao {

    public boolean login(String username, String password, HttpSession session) throws SQLException {
        Database db = new Database();
        db.connect();
        try {
            Statement stmt = db.connection.createStatement();



                String query = "select * from admin where username like '" + username + "'";
                ResultSet rs = stmt.executeQuery(query);
                while (rs.next() == true && EncryptionUtil.matchWithSecuredHash(password, rs.getString("password"))) {
                    session.setAttribute("userId", rs.getString("id"));
                    session.setAttribute("username", username);
                    session.setAttribute("screenName", username);
                    db.close();
                    return true;
                }

        }catch(Exception e){
        }finally {
            db.close();
        }

        return false;
    }

    public void setPass(String username, String hashedPass) throws SQLException {
        Database db = new Database();
        db.connect();
        try {
            Statement stmt = db.connection.createStatement();
            String query = "update admin set password='" + hashedPass + "' where username like '" + username + "'";
            stmt.executeUpdate(query);
        }catch (Exception e) {

        }finally {
            db.close();
        }

        return;
    }

    public boolean register(String username, String password, HttpSession session) throws SQLException {
        Database db = new Database();
        db.connect();
        try {

            Statement st2 =db.connection.createStatement();
            String q2="select max(id)+1 as mx from admin";
            ResultSet rs2=st2.executeQuery(q2);
            rs2.next();


            String hashedPass = EncryptionUtil.generateSecuredHash(password);

            Statement stmt = db.connection.createStatement();

            String query = "INSERT into admin (id,username,password) values("+rs2.getString("mx")+",'"+username+"','"+hashedPass+"')";
            stmt.executeUpdate(query);
            return true;

        }catch(Exception e){
        }finally {
            db.close();
        }

        return false;
    }
}
