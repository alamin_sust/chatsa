-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: chatsa
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(110) NOT NULL,
  `password` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_username_uindex` (`username`),
  UNIQUE KEY `admin_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (0,'0','0'),(1,'admin','$2a$12$wtFehU6xU9mwTbb79ngPrOe3iJkHWNW2Cio3jH2LOVA3JRiUT2Cr2'),(2,'md. al-amin','$2a$12$u4gAAPlvUzApceFvvu3n7OFqKyYFjZrBf3mVbLZhZBJKLisPDNqAu');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `anonymous`
--

DROP TABLE IF EXISTS `anonymous`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anonymous` (
  `id` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `anonymous_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anonymous`
--

LOCK TABLES `anonymous` WRITE;
/*!40000 ALTER TABLE `anonymous` DISABLE KEYS */;
INSERT INTO `anonymous` VALUES (0,1),(1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1),(37,1),(38,1),(39,1),(40,1),(41,1),(42,1),(43,1),(44,1),(45,1),(46,1),(47,1),(48,1),(49,1),(50,1),(51,1),(52,1),(53,1),(54,1),(55,1),(56,1),(57,1),(58,1),(59,1),(60,1),(61,1),(62,1),(63,1),(64,1),(65,1),(66,1),(67,1),(68,1),(69,1),(70,1),(71,1),(72,1),(73,1),(74,1),(75,1),(76,1),(77,1),(78,1),(79,1),(80,1),(81,1),(82,1);
/*!40000 ALTER TABLE `anonymous` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `area`
--

DROP TABLE IF EXISTS `area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area` (
  `id` int(11) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `area_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `area`
--

LOCK TABLES `area` WRITE;
/*!40000 ALTER TABLE `area` DISABLE KEYS */;
INSERT INTO `area` VALUES (1,'Bellville',1),(2,'Philadelphia',1),(3,'Durbanville',1),(4,'Hillbrow',2),(5,'Kensington',2),(6,'Parktown',2),(7,'ga kgapane',6),(8,'tzaneen',6),(9,'mamelodi',4);
/*!40000 ALTER TABLE `area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat`
--

DROP TABLE IF EXISTS `chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `city` int(11) DEFAULT '0',
  `area` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `message` varchar(1010) DEFAULT NULL,
  `session_id` int(11) DEFAULT NULL,
  `name` varchar(110) DEFAULT NULL,
  `datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `chat_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat`
--

LOCK TABLES `chat` WRITE;
/*!40000 ALTER TABLE `chat` DISABLE KEYS */;
INSERT INTO `chat` VALUES (0,0,0,NULL,NULL,NULL,NULL,'2017-04-29 12:27:49'),(1,0,0,NULL,'JYY',3,'anonymous4','2017-04-29 12:27:49'),(2,0,0,NULL,'hmm',3,'anonymous4','2017-04-29 12:30:22'),(3,0,0,NULL,'hi',4,'anonymous5','2017-04-29 12:30:51'),(4,0,0,NULL,'ok?',4,'Samiha','2017-04-29 12:31:42'),(5,0,0,NULL,'Will I set my name too?',3,'anonymous4','2017-04-29 12:31:59'),(6,0,0,NULL,'Yes vaia',4,'Samiha','2017-04-29 12:32:09'),(7,0,0,NULL,'YEY',3,'Al- Amin','2017-04-29 12:32:26'),(8,0,0,NULL,'OK',4,'Samiha','2017-04-29 12:32:32'),(9,0,0,NULL,'holo',4,'Samiha','2017-04-29 12:41:42'),(10,0,0,NULL,'okay',3,'Al- Amin','2017-04-29 12:41:58'),(11,0,0,NULL,'holo????????',3,'Al- Amin','2017-04-29 12:42:13'),(12,0,0,NULL,'shahad ki koro?',6,'anonymous7','2017-04-29 12:43:21'),(13,0,0,NULL,'aito',7,'anonymous8','2017-04-29 12:43:30'),(14,0,0,NULL,'oceans',6,'anonymous7','2017-04-29 12:45:41'),(15,0,0,NULL,'hi shahad',13,'anonymous14','2017-04-29 13:42:08'),(16,0,0,NULL,'ki koro',13,'anonymous14','2017-04-29 13:42:27'),(17,0,0,NULL,'hi dad',16,'anonymous17','2017-04-29 13:55:49'),(18,0,0,NULL,'hmm bolo',15,'anonymous16','2017-04-29 13:56:00'),(19,1,1,NULL,'hi',15,'anonymous16','2017-04-29 14:15:37'),(20,1,1,NULL,'hiuuu',15,'anonymous16','2017-04-29 14:16:05'),(21,2,5,NULL,'oooo',15,'anonymous16','2017-04-29 14:16:58'),(22,1,0,NULL,'asdfgasdfasdfasdfa',15,'anonymous16','2017-04-29 14:18:27'),(23,1,0,NULL,'oyeeeee',15,'anonymous16','2017-04-29 14:24:48'),(24,0,0,NULL,'dyhghgh',15,'anonymous16','2017-04-29 14:26:33'),(25,1,0,NULL,'dfgsdfgsdfgsdfgsdfgsdfgsdafgsdfgsdfgsd',15,'anonymous16','2017-04-29 14:28:59'),(26,1,0,NULL,'cape',15,'anonymous16','2017-04-29 14:37:15'),(27,0,0,NULL,'global',15,'anonymous16','2017-04-29 14:37:26'),(28,1,3,NULL,'cape durban',15,'anonymous16','2017-04-29 14:37:37'),(29,3,9,NULL,'Hello',26,'anonymous27','2017-04-29 18:55:00'),(30,3,9,NULL,'Yey',28,'anonymous30','2017-04-29 18:55:23'),(31,3,9,NULL,'a',26,'anonymous27','2017-04-29 18:55:34'),(32,3,9,NULL,'a',26,'anonymous27','2017-04-29 18:55:37'),(33,3,9,NULL,'a',26,'anonymous27','2017-04-29 18:55:40'),(34,3,9,NULL,'a',26,'anonymous27','2017-04-29 18:55:42'),(35,3,9,NULL,'AAA',26,'anonymous27','2017-04-29 18:55:47'),(36,3,9,NULL,'AAA',26,'anonymous27','2017-04-29 18:55:49'),(37,3,9,NULL,'asd',26,'anonymous27','2017-04-29 18:55:58'),(38,0,0,NULL,'ererqw',26,'anonymous27','2017-04-29 19:00:58'),(39,0,0,NULL,'hi',32,'anonymous36','2017-04-29 20:03:25'),(40,0,0,NULL,'Hello everyone',33,'Joe','2017-04-29 20:08:17'),(41,0,0,NULL,'TA',33,'anonymous37','2017-04-29 20:10:12'),(42,0,0,NULL,'Hello Thapelo Brother :)',32,'anonymous36','2017-04-29 20:10:20'),(43,2,4,NULL,'Nice',33,'Live','2017-04-29 20:19:28'),(44,0,0,NULL,'How are you there',33,'Thapz','2017-04-29 20:20:25'),(45,0,0,NULL,'hi',32,'qwertyuiopasdfghjkl','2017-04-29 20:50:10'),(46,4,9,NULL,'mamelodi',39,'ano43','2017-04-29 23:30:09'),(47,6,7,NULL,'ola',39,'ano43','2017-04-29 23:42:34');
/*!40000 ALTER TABLE `chat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(110) DEFAULT NULL,
  `type` varchar(110) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `city_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (1,'Cape Town','city'),(2,'Johannesburg','city'),(3,'Durban','city'),(4,'Pretoria','city'),(5,'Port Elizabeth','city'),(6,'Limpopo','city'),(7,'Cape Town University','university'),(8,'General Room 1','general');
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session` (
  `id` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `session_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session`
--

LOCK TABLES `session` WRITE;
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
INSERT INTO `session` VALUES (0,1),(1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1),(37,1),(38,1),(39,1),(40,1),(41,1),(42,1),(43,1),(44,1),(45,1),(46,1),(47,1),(48,1),(49,1),(50,1),(51,1),(52,1),(53,1),(54,1),(55,1),(56,1),(57,1),(58,1),(59,1),(60,1),(61,1),(62,1),(63,1),(64,1),(65,1),(66,1),(67,1),(68,1),(69,1),(70,1),(71,1),(72,1),(73,1),(74,1),(75,1),(76,1),(77,1);
/*!40000 ALTER TABLE `session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(110) NOT NULL,
  `password` varchar(110) DEFAULT NULL,
  `firstname` varchar(110) DEFAULT NULL,
  `lastname` varchar(110) DEFAULT NULL,
  `city` int(11) DEFAULT NULL,
  `area` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_username_uindex` (`username`),
  UNIQUE KEY `user_id_uindex` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-06  5:40:10
